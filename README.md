A puzzle visualization.  Partitions a rectangular image into squares, displays piles of squares on screen.  Each square is animated to its original position in the image.  Intended to be used during training sessions for SSVEP.

NOTE: must have https://bitbucket.org/cogsyslab/psychtoolboxobjects module on path.  (We haven't included as submodule to avoid including it twice)

markbhigger@gmail.com, matt.higger@gmail.com Sept-23-2015