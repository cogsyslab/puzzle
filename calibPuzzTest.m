clc; clearvars;

%% params

screenNumber = 0;

% either 'dog.jpg' or 'HawksBill_Turtle.jpg'
imagePath = 'HawksBill_Turtle.jpg';

% posRatios of each fragPile
fragPilePosRatio = ...
    [0,     0,      .2,     .2; ...
    .8,     0,      1,      .2; ...
    0,      .8,     .2,     1; ...
    .8,     .8,     1,      1];

fragPileMinFrag = [10, 9, 8, 7]';

%% init
calibPuzzObj = calibPuzz(imagePath, ...
    fragPilePosRatio, ...
    fragPileMinFrag, ...
    'screenNumber', screenNumber, ...
    'screen2ImageRatio', 1, ... 
    'posRatio', [0, 0, .8, 1], ...
    'animTime', .2);


%% test
% calibPuzzObj.ResetFrag(fragPilePosRatio, fragPileMinFrag);
black = [0, 0, 0, 255];
Screen('FillRect', calibPuzzObj.windowPointer, black, calibPuzzObj.pos);
calibPuzzObj.Draw;
calibPuzzObj.Flip;
pause;
% should "deal" 4 from each pile in a row, starting top left and clockwise
for pileIdx = 1 : 4
    for idx = 1 : 7
        calibPuzzObj.Animate(pileIdx);
    end
end
pause;
calibPuzzObj.Animate(2, 'timeSec', 1)
calibPuzzObj.AnimateRest('timeSec', 1);
pause;
sca;
